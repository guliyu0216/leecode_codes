class Solution(object):
    def longestPalindrome(self, s):
        """
        :type s: str
        :rtype: str
        """

        n = len(s)
        maxLen = 0
        maxL = 0
        maxR = 0
        dp = [[False] * n for i in range(n)]

        for r in range(n):
            for l in range(n):
                if r == l:
                    dp[l][r] = True
                elif r - l == 1:
                    dp[l][r] = True if s[l] == s[r] else False
                elif r - l > 1:
                    dp[l][r] = True if dp[l+1][r-1] and s[l] == s[r] else False
                else:
                    continue
                if dp[l][r] and r - l > maxLen:
                    maxLen = r - l
                    maxL = l
                    maxR = r

        return s[maxL:maxR+1]

s = Solution()
print(s.longestPalindrome("cbbd"))




